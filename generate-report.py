import csv
import json
import logging
import time

import requests

from main import get_objects, load_api_key, MODELS

from logger import logger

def load_stored_urls():
    try:
        with open("uploaded_schemas.json", "r") as f:
            return json.load(f)
    except FileNotFoundError:
        return {}


def save_stored_urls(stored_urls):
    with open("uploaded_schemas.json", "w") as f:
        json.dump(stored_urls, f)


def upload_file_to_fileio(file_content):
    upload_response = requests.post("https://file.io", files={"file": ("file.txt", file_content)})

    if upload_response.status_code == 200:
        upload_data = upload_response.json()
        return upload_data["link"]
    else:
        return None


def process_schemas(local_schemas):
    stored_urls = load_stored_urls()

    for local_schema in local_schemas:
        if local_schema not in stored_urls:
            uploaded_url = upload_file_to_fileio(open(local_schema, "rb").read())
            if uploaded_url is not None:
                stored_urls[local_schema] = uploaded_url

    save_stored_urls(stored_urls)
    return {local_schema: requests.get(url).text for local_schema, url in stored_urls.items()}


# Define the local schema files
local_schemas = [
    "schemas/testmap.avsc",
    "schemas/medium-document.jcs",
    "schemas/jsonparsedschema.jcs",
    "schemas/complex-document.jcs"
]

load_api_key()

schemas = process_schemas(local_schemas)

# Define the number of items
num_items_list = [1, 3, 5, 10]

# Log the summary of the execution
logger.info(f"Starting execution with the following parameters:")
logger.info(f"Models: {list(MODELS.keys())}")
logger.info(f"Schemas: {list(schemas.keys())}")
logger.info(f"Number of items: {num_items_list}")
logger.info("""For each combination of model, schema, and number of items, an execution will \
be performed and its result will be recorded.""")

# Open the CSV file
with open('report.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    # Write the header
    writer.writerow(["Model", "Schema", "Num_Items", "Time", "Cost", "Response_URL", "Error"])

    total_time = 0
    total_cost = 0

    # Iterate over all combinations of models, schemas, and num_items
    for model in MODELS.keys():
        for local_schema, schema in schemas.items():
            num_items = None
            try:
                for num_items in num_items_list:
                    error = None
                    try:
                        # Log the start of the execution
                        logger.info(f"Starting execution for model {model}, schema {local_schema}, num_items {num_items}")
                        # Measure the start time
                        start_time = time.time()
                        # Execute the main function
                        objects, cost = get_objects(schema, num_items, model=model)
                        # Measure the end time
                        end_time = time.time()
                        # Calculate the elapsed time
                        elapsed_time = end_time - start_time
                        total_time += elapsed_time
                        total_cost += cost
                        # Upload the response to file.io
                        response_url = upload_file_to_fileio(json.dumps(objects))
                    except Exception as e:
                        error = str(e)
                        logger.error(f"Error generating objects for model {model}, schema {local_schema}, num_items {num_items}: {e}")
                    finally:
                        # Write the results to the CSV file
                        writer.writerow([model, local_schema, num_items, elapsed_time, cost, response_url, error])
                        # Log the end of the execution
                        logger.info(f"Finished execution for model {model}, schema {local_schema}, num_items {num_items}. Time: {elapsed_time} seconds. Cost: {cost}")
            except Exception as e:
                logger.error(f"Error generating objects for model {model}, schema {local_schema}, num_items {num_items}: {e}")

    logger.info(f"Total execution time: {total_time} seconds and Total cost: {total_cost}")


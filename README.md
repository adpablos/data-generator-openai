# Data Generator OpenAI
Data Generator OpenAI is a Python application that uses OpenAI's GPT-3 and GPT-4 models to generate JSON objects based on provided JSON schemas. It can generate multiple objects at once and supports a variety of models. The application also includes a utility for generating a performance report, which measures the time and cost of generating objects using different models and schemas.

For a detailed explanation of the project, its conclusions, and more, check out this [blog post](https://alexdepablos.com/en/software-engineering/synthetic-data-generation/).

## Installation
1. Clone the repository:
```
git clone https://github.com/yourusername/data-generator-openai.git
```
2.  Navigate to the project directory:
```
cd data-generator-openai
```
3.  Install the required dependencies:

```pip install -r requirements.txt```

## Usage
### API Server
The API server is implemented using FastAPI. To start the server, run:

```uvicorn app:app --reload```

The server provides a single endpoint, /generate, which accepts POST requests. The request body should be a JSON object with the following properties:

- `schema_spec`: The JSON schema to generate objects from. This can be a URL, a file path, or a JSON string.
- `num_objects`: The number of objects to generate. This should be an integer between 1 and 5.
- `temperature`: The temperature to use when generating objects. This should be a float between 0 and 1.
- `prompt`: An optional prompt to use when generating objects.
- `model`: The model to use when generating objects. This should be one of the following: gpt-4, gpt-4-32K, text-davinci-003, text-curie-001, text-babbage-001, text-ada-001, gpt-3.5-turbo.

The response will be a JSON object with the following properties:

- `objects`: An array of generated objects.
- `cost`: The cost of generating the objects.

### Performance Report
The generate-report.py script generates a performance report, which measures the time and cost of generating objects using different models and schemas. The script generates objects for all combinations of models, schemas, and number of items, and writes the results to a CSV file.

The script will read JSON schemas from the schemas/ directory. You can add your own schemas to this directory, and the script will automatically pick them up.

To run the script, execute:

```python generate-report.py```

The script generates two output files:

- `report.csv`: This file contains the execution time and cost for each combination of model, schema, and number of items. It includes the model used, the schema, the number of items generated, the time taken, the cost, the URL of the generated data, and any errors that occurred.
  - `Model`: The model used to generate objects.
  - `Schema`: The schema used to generate objects.
  - `Num_Items`: The number of objects generated.
  - `Time`: The time taken to generate the objects.
  - `Cost`: The cost of generating the objects.
  - `Response_URL`: The URL of the generated objects on file.io.
  - `Error`: Any error that occurred while generating the objects.

- `uploaded_schemas.json`: This file contains the URLs of the uploaded schemas. The schemas are uploaded to file.io for each run of the script, and the URLs are stored in this file.

## Configuration
The application requires an OpenAI API key, which should be provided in an environment variable named OPENAI_API_KEY. You can set this variable in a .env file in the project root directory.

## Contributing
Contributions are welcome! Please submit a pull request or create an issue to propose changes or additions.

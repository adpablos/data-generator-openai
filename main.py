from logger import logger

import time

import openai
import os
from dotenv import load_dotenv, find_dotenv
import json
import requests
from fastapi import HTTPException


MODELS = {
            "gpt-4": {
                "description": "GPT-4 8K Context",
                "prompt_cost_per_token": 0.03 / 1000,
                "completion_cost_per_token": 0.06 / 1000,
                "type": "ChatCompletion"
            },
            "gpt-4-32K": {
                "description": "GPT-4 32K Context",
                "prompt_cost_per_token": 0.06 / 1000,
                "completion_cost_per_token": 0.12 / 1000,
                "type": "ChatCompletion"
            },
            "text-davinci-003": {
                "description": "Davinci",
                "prompt_cost_per_token": 0.02 / 1000,
                "completion_cost_per_token": None,
                "type": "Completion"
            },
            "text-curie-001": {
                "description": "Curie",
                "prompt_cost_per_token": 0.002 / 1000,
                "completion_cost_per_token": None,
                "type": "Completion"
            },
            "text-babbage-001": {
                "description": "Babbage",
                "prompt_cost_per_token": 0.0005 / 1000,
                "completion_cost_per_token": None,
                "type": "Completion"
            },
            "text-ada-001": {
                "description": "Ada",
                "prompt_cost_per_token": 0.0004 / 1000,
                "completion_cost_per_token": None,
                "type": "Completion"
            },
            "gpt-3.5-turbo": {
                "description": "GPT-3.5 Turbo",
                "prompt_cost_per_token": 0.0015 / 1000,
                "completion_cost_per_token": 0.002 / 1000,
                "type": "ChatCompletion"
            }
        }


class IncompleteJsonResponseError(Exception):
    def __init__(self, message, incomplete_json):
        super().__init__(message)
        self.incomplete_json = incomplete_json

    def __str__(self):
        return f"{super().__str__()} Incomplete JSON: {self.incomplete_json}"


def load_api_key():
    load_dotenv(find_dotenv())
    openai.api_key = os.getenv('OPENAI_API_KEY')


def get_schema(value):
    schema = value
    # Comprueba si el esquema es una URL
    if schema.startswith('http://') or schema.startswith('https://'):
        try:
            response = requests.get(schema)
            response.raise_for_status()
            return response.text
        except requests.RequestException:
            raise HTTPException(status_code=400, detail="No se pudo obtener el esquema de la URL proporcionada")
    # Si no es una url y no empieza por {, entonces entendemos que es un path de un fichero
    elif not schema.startswith('{'):
        try:
            with open(value, "r") as f:
                return f.read()
        except FileNotFoundError:
            raise HTTPException(status_code=400, detail="No se pudo obtener el esquema del fichero proporcionado")


def get_openai_response(model, prompt, temperature=0):
    if MODELS[model]["type"] == "ChatCompletion":
        return get_chat_completion(model=model, prompt=prompt, temperature=temperature)
    elif MODELS[model]["type"] == "Completion":
        return get_completion(model=model, prompt=prompt, temperature=temperature)
    else:
        raise ValueError(f"The model {model} is not recognized.")


def extract_json_from_response(content):
    try:
        # Find the start and end of the JSON array
        start = content.find("```json") + len("```json")
        end = content.rfind("```")

        # Extract the JSON array and load it as a Python object
        json_string = content[start:end].strip()
        logger.debug(f"Attempting to parse JSON string: {json_string}")

        if not (json_string.startswith('[') and json_string.endswith(']')):
            raise IncompleteJsonResponseError("The OpenAI API response does not contain a complete JSON string.", content[start:end].strip())

        json_array = json.loads(json_string)
    except Exception as e:
        # Log the error and raise an exception if something goes wrong
        logger.error(f"Failed to extract JSON array from OpenAI API response: {e}")
        raise

    return json_array


def get_chat_completion(prompt, model="gpt-3.5-turbo", temperature=0):
    messages = [{"role": "user", "content": prompt}]
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=temperature,
    )

    content = response.choices[0].message["content"]
    return extract_json_from_response(content), calculate_cost(response, model)


def get_completion(prompt, model="text-ada-001", temperature=0):
    response = openai.Completion.create(
        engine=model,
        prompt=prompt,
        temperature=temperature,
    )
    content = response.choices[0].text
    return extract_json_from_response(content), calculate_cost(response, model)


def get_objects(schema, desired_number_of_objects, temperature=0.0, prompt=None, model="gpt-3.5-turbo"):
    logger.info(f"Generating {desired_number_of_objects} objects using model {model}...")
    if prompt is None:
        prompt = """From the following schema delimited by triple backticks:

        Schema:
        ```{schema}```

        Instructions:
        1. Build a json object that adheres to the given schema.
        2. Generate an array of {desired_number_of_objects} data objects of that previous json object.
        3. Ensure coherence and correlation between related fields.
        4. Fields should have valid values as it they were real data. 
        5. Phone numbers must have the correct number of digits.
        6. Address should be with a valid zip code, etc.
        7. We could have objects from different types in the same array. Meaning that if a field is country, \
         it could be US, ES, or any other country in the world, but the rest of the fields should be coherent \
         with the country.

        Please generate the required data objects in a json array format and enclose the JSON array within \
        triple backticks and prefix it with `json`."""
        prompt = prompt.format(schema=schema, desired_number_of_objects=desired_number_of_objects)
    return get_openai_response(prompt=prompt, model=model, temperature=temperature)


def calculate_cost(open_ai_api_response, model):
    if model not in MODELS:
        logger.error(f"Unknown model: {model}. Can't calculate cost.")
        return None

    if open_ai_api_response is None:
        logger.error("There was not a valid OpenAI API response. Can't calculate cost.")
        return None

    model = MODELS[model]
    prompt_tokens = open_ai_api_response['usage']['prompt_tokens']
    completion_tokens = open_ai_api_response['usage']['total_tokens']

    prompt_cost = prompt_tokens * model["prompt_cost_per_token"]
    completion_cost = completion_tokens * model["completion_cost_per_token"]

    total_cost = prompt_cost + completion_cost
    return total_cost


def processing_time(start_time):
    minutes, seconds = divmod(time.time() - start_time, 60)
    return f"{int(minutes):02d}:{int(seconds):02d}"


if __name__ == '__main__':
    start_time = time.time()
    load_api_key()
    objects, cost = get_objects(get_schema("schemas/complex-document.jcs"), 3)
    logger.info(f"Objects generated: {objects}")
    logger.info(f"Processing cost: ${cost:.6f}")
    logger.info(f"Processing time: {processing_time(start_time)}")

import logging

logger = logging.getLogger('data-generator-openai')
logger.setLevel(logging.INFO)

FORMAT = '%(asctime)s - %(levelname)s - %(message)s'

handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(FORMAT))
logger.addHandler(handler)

from typing import Optional

import requests
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel, validator

import main

main.load_api_key()

app = FastAPI()


class SchemaInput(BaseModel):
    schema_spec: str
    num_objects: int = 1
    temperature: Optional[float] = 0.0
    prompt: Optional[str] = None
    model: Optional[str] = "gpt-3.5-turbo"

    @validator('num_objects')
    def validate_num_objects(cls, v):
        if v < 1 or v > 5:
            raise ValueError('num_objects debe estar entre 1 y 5')
        return v

    @validator('schema_spec')
    def validate_schema(cls, v):
        if not (v.startswith('http://') or v.startswith('https://') or v.startswith('{')):
            raise ValueError('schema debe ser una URL o un esquema que comienza con "{"')
        return v

    @validator('model')
    def validate_model(cls, v):
        if v not in main.MODELS.keys():
            raise ValueError(f'model debe ser uno de los siguientes: {", ".join(main.MODELS.keys())}')
        return v


@app.post("/generate")
async def generate(input: SchemaInput):
    schema = input.schema_spec
    num_objects = input.num_objects
    temperature = input.temperature
    prompt = input.prompt
    model = input.model

    # Comprueba si el esquema es una URL
    if schema.startswith('http://') or schema.startswith('https://'):
        try:
            response = requests.get(schema)
            response.raise_for_status()
            schema = response.text
        except requests.RequestException:
            raise HTTPException(status_code=400, detail="No se pudo obtener el esquema de la URL proporcionada")

    try:
        objects, cost = main.get_objects(schema, num_objects, temperature, prompt, model)
    except main.IncompleteJsonResponseError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

    return {
        'objects': objects,
        'cost': cost
    }
